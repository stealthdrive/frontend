import React from 'react';

const backendHost = "stealthdrive.jikaven.id.lv";

const UploadButton = () => {
  const handleFileSelect = (event) => {
    const formData = new FormData();
    formData.append('file', event.target.files[0]);
    fetch(`https://${backendHost}/api/upload`, {
      method: 'POST',
      body: formData
    })
    .then(response => {
      // Handle response
      console.log(response)
    })
    .catch(error => {
      // Handle error
      console.log(error)
    });
  };

  return (
    <form
    action="/api/upload"
    method="post"
    encType="multipart/form-data" // Required for file uploads
    className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800 hover:dark:bg-opacity-30 cursor-pointer"
  >
    <label htmlFor="file" className="block">
      <h2 className="mb-3 text-2xl font-semibold">Upload</h2>
      <p className="m-0 max-w-[30ch] text-sm opacity-50">
        Push your license plate image
      </p>
      <input
        type="file"
        id="file"
        name="file"
        className="hidden"
        accept="image/*" // Specify the types of files that the input should accept
        onChange={handleFileSelect} // Automatically submit form when file is selected
      />
    </label>
    <button type="submit" className="hidden">Upload</button>
  </form>
  );
};

export default UploadButton;
