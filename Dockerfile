FROM node:21-alpine as base

# =====

FROM base AS dependencies

WORKDIR /app

RUN apk update --no-cache && \
    apk upgrade --no-cache && \
    apk add --no-cache libc6-compat && \
    rm -rf /var/cache/apk/*

COPY package*.json ./

RUN npm ci

# =====

FROM base as runner

WORKDIR /app
ENV NODE_ENV production
ENV HOSTNAME "0.0.0.0"
ENV NEXT_TELEMETRY_DISABLED 1

RUN apk update --no-cache && \
    apk upgrade --no-cache

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY app ./app
COPY public ./public
COPY package*.json postcss.config.js next.config.mjs tailwind.config.js .env? ./
COPY --from=dependencies /app/node_modules ./node_modules

RUN npm run build
RUN chown nextjs:nodejs ./.next

USER nextjs

ENTRYPOINT ["npm", "run"]
CMD ["start"]
